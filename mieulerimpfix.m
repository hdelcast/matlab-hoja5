function [T,Y,ev,loopcount]=mieulerimpfix(f,intv,y0,N,TOL,nmax,jfunc)

a = intv(1);
b = intv(end);
h = (b - a)/N;

ev = 0;
loopcount = 0;

t = a;
y = y0;

T = t;
Y = y;

for i=1:N 

    % t
    t = t + h;
    
    % función auxiliar
    F = @(z) y + h * f(t,z);

    % iteración punto fijo
    [y, ev0,loop] = mifixsystem(F,y,TOL,nmax);

    % vectores
    T = [T, t];
    Y = [Y, y];

    % evaluaciones y búcles
    ev = ev + ev0; % iter punto fijo
    loopcount = loopcount + loop; % iter punto fijo

end
end