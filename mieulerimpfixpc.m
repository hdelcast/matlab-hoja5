function [T,Y,ev,loopcount]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax,jfunc)

a = intv(1);
b = intv(end);
h = (b - a)/N;

ev = 0;
loopcount = 0;

t = a;
y = y0;

T = t;
Y = y;

for i=1:N
   
    % cálculo y(0) método euler (P-C)
    y1 = y + h * f(t,y);

    % t(i+1)
    t = t + h;

    % función auxiliar
    F = @(z) y + h * f(t,z);

    % iteración punto fijo
    [y, ev0, loop] = mifixsystem(F,y1,TOL,nmax);

    % vectores
    T = [T, t];
    Y = [Y, y];

    % evaluaciones y búcles
    ev = ev + 1; % eval para P-C
    ev = ev + ev0; % eval punto fijo
    loopcount = loopcount + loop;

end
end