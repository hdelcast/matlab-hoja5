function [T,Y,ev,loopcount]=mieulerimpnwt2(f,intv,y0,N,TOL,nmax,jfunc)

a = intv(1);
b = intv(end);
h = (b - a)/N;

ev = 0;
loopcount = 0;

t = a;
y = y0;

T = t;
Y = y;

for i=1:N

    % t
    t = t + h;
    
    % función auxiliar
    F = @(z) z - (y + h * f(t,z));

    % jacobiano
    Jf = @(z) eye(length(z)) - h * jfunc(z);
    %Jf = eye(length(y)) - h * jfunc;
 
    % iteración newton
    [y,ev0,loop]=minewtonsystem(F,Jf,y,TOL,nmax);

    % vectores
    T = [T, t];
    Y = [Y, y];

    % evaluaciones y búcles
    ev = ev + ev0;
    loopcount = loopcount + loop;

end
end