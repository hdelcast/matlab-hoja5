function [T,Y,ev,loopcount]=mieulerimpnwtpc2(f,intv,y0,N,TOL,nmax,jfunc)

a = intv(1);
b = intv(end);
h = (b - a)/N;

ev = 0;
loopcount = 0;

t = a;
y = y0;

T = t;
Y = y;

for i=1:N

    % cálculo y(0) método euler (P-C)
    y1 = y + h * f(t,y);

    % t(i+1)
    t = t + h;
    
    % función auxiliar
    F = @(z) z - (y + h * f(t,z));

    % jacobiano
    Jf = @(z) eye(length(z)) - h * jfunc(z);
    %Jf = eye(2) - h * jfunc;

    % iteración newton
    [y,ev0,loop]=minewtonsystem(F,Jf,y1,TOL,nmax);

    % vectores
    T = [T, t];
    Y = [Y, y];

    % evaluaciones y bucles
    ev = ev + 1; % eval para P-C
    ev = ev + ev0; % eval newton
    loopcount = loopcount + loop;
end
end