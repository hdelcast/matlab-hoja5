function [y,ev,loop]=mifixsystem(f,y0,TOL,nmax) 

y = y0;
loop = 0;
ev = 0;
dif = 1;
%dif = TOL;

while (dif > TOL) && loop < nmax
    k = f(y);
    dif = max(max(abs(y - k)));
    loop = loop + 1;
    ev = ev + 1;
    y = k;
end
end

