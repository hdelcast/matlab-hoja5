function [y,ev,loop]=minewtonsystem(f,Jf,y0,TOL,nmax)

y = y0;
loop = 0;
ev = 0;
dif = 1;

while (dif > TOL) && loop < nmax
    k = y - Jf(y)\f(y);
    %k = y - f(y)/Jf;
    dif = max(max(abs(y-k)));
    %dif = max(max(k));
    loop = loop + 1;
    ev = ev + 2;
    y = k;
end
end