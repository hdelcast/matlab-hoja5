function [y,ev,loop]=minewtonsystem(f,Jf,y0,TOL,nmax)

y = y0;
loop = 0;
ev = 0;
dif = 1;

while (dif >= 0.005) && loop < nmax
    w = Jf\f;
    dif = norm(w);
    loop = loop + 1;
    ev = ev + 1;
    y = y - w;

end
end