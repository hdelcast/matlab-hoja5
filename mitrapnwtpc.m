function [t,Y,ev,loopcount]=mitrapnwtpc(f,intv,y0,N,TOL,nmax,jfunc)

ev = 0;
loopcount = 0;

a = intv(1);
b = intv(end);
h = (b - a)/N;
t = a:h:b;

m = size(y0, 1);
Y = zeros(m, length(t));

ev = 0;
loopcount = 0;

Y(:,1) = y0;

for i=1:N
    loop = 0;
    dif = 1;

    % Predictor (Euler)
    y = Y(:,i) + h * feval(f,t(i),Y(:,i)); 

    while (dif >= h^3) && (loop < nmax)

        % Corrector (Newton)
        % función auxiliar
        F = y - Y(:,i) - (h/2) * feval(f, t(i), Y(:,i)) - (h/2) * feval(f,t(i+1),y);

        % jacobiano
        Jf = eye(length(y0)) - (h/2) * feval(jfunc, t(i+1), y);
        
        % Newton
        w = Jf\F;
        y = y - w;
        dif = norm(w);

        % evaluaciones y bucles
        loop = loop + 1;
        ev = ev + 3;

    end 
    Y(:, i+1) = y;
    loopcount = loopcount + loop;
end
end