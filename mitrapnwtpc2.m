function [T,Y,ev,loopcount]=mitrapnwtpc2(f,intv,y0,N,TOL,nmax,jfunc)

a = intv(1);
b = intv(end);
h = (b - a)/N;

ev = 0;
loopcount = 0;

t = a;
y = y0;

T = t;
Y = y;

for i=1:N
    
    % evaluación f
    f1 = f(t,y);

    % cálculo y(0) método euler (P-C)
    y1 = y + h * f1;

    % t
    t = t + h;
    
    % función auxiliar
    F = @(z) z - (y + (h/2) * (f1 + f(t,z)));

    % jacobiano
    Jf = @(z) eye(length(z)) - h * jfunc(z);
    %Jf = eye(2) - h * jfunc;

    % iteración newton
    [y,ev0,loop]=minewtonsystem(F,Jf,y1,TOL,nmax);

    % vectores
    T = [T, t];
    Y = [Y, y];

    % evaluaciones y búcles
    ev = ev + 1; % evaluación f1
    ev = ev + ev0; % iter newton
    loopcount = loopcount + loop; % iter newton

end
end