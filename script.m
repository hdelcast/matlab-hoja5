% Datos iniciales del problema
A = [-2 1; 1 -2];
B = @(t) [2*sin(t); 2*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
%f = @(t,y) [ -2 * y(1) + y(2) + 2 * sin(t) ; y(1) - 2 * y(2) + 2 * (cos(t) - sin(t))];
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
intv=[0 10];
y0=[2;3];
nmax = 10;
TOL = .001;
N0 = 100;
h0 = 0.1;
jfunc = @(t) A;
%jfunc = A;

% inicialización vectores (h y N)
N0 = 100;
h0 = 0.1;
nVec = N0;
hVec = h0;
errorVec = zeros(1,8);

for j=1:8
    nVec(j)=N0*2^(j);
    hVec(j)=10/nVec(j);
end 

%% Forma 1
% Funciones de los métodos y sus nombres
% mieulerimpfix vs mieulerimpfixpc
mFuncs = {@mieulerimpfix, @mieulerimpfixpc};
mNames = {'mieulerimpfix', 'mieulerimpfixpc'};
% mieulerimpnwt vs mieulerimpfixpc
%mFuncs = {@mieulerimpnwt, @mieulerimpfixpc};
%mNames = {'mieulerimpnwt', 'mieulerimpfixpc'};
% mieulerimpnwt vs mieulerimpnwtpc
%mFuncs = {@mieulerimpnwt, @mieulerimpnwtpc};
%mNames = {'mieulerimpnwt', 'mieulerimpnwtpc'};


% inicialización figura
figure1 = figure;
lines = ["*-", ">-", "^-"];

% evaluación error de cada método
for j=1:length(mFuncs)
    for i=1:length(nVec)

        % cálculo error global junto con número de evaluaciones
        [error, eval] = mierrorglobal(mFuncs{j},f,f_exact,intv,y0,nVec(i),TOL,nmax,jfunc);
        errorVec(i) = error;
    end
    
    %comprobación error
    p2 = sprintf('%g ', errorVec);
    fprintf('El vector de error para el método "%s" es: \n \n [%s] \n \n ', mNames{j}, p2)
    
    %plot
    figure(figure1);
    grid on;
    loglog(hVec,errorVec,lines{j});
    hold on;

end


%% Forma 2
% inicialización figura
figure1 = figure;
lines = ["*-", ">-", "^-"];

%%% evaluación error de mieulerimpfix
err1 = [];
for i=1:length(nVec)
    % cálculo error global junto con número de evaluaciones
    [error, eval] = mierrorglobal(@mieulerimpfix,f,f_exact,intv,y0,nVec(i),TOL,nmax,jfunc);
    err1(i) = error;
end

%comprobación error
p2 = sprintf('%g ', err1);
fprintf('El vector de error para el método "%s" es: \n \n [%s] \n \n ', 'mieulerimpfix', p2)

%%% evaluación error de mieulerimpfixpc
err2 = [];
for i=1:length(nVec)
    % cálculo error global junto con número de evaluaciones
    [error, eval] = mierrorglobal(@mieulerimpfixpc,f,f_exact,intv,y0,nVec(i),TOL,nmax,jfunc);
    err2(i) = error;
end

%comprobación error
p3 = sprintf('%g ', err2);
fprintf('El vector de error para el método "%s" es: \n \n [%s] \n \n ', 'mieulerimpfixpc', p3)

%plot
figure(figure1);
grid on;
loglog(hVec,err1,lines{1},hVec,err2,lines{2});
legend('mieulerimpfix','mieulerimpfixpc','Location','northeast');
hold on;

%%

A = [-2 1; 1 -2];
B = @(t) [2*sin(t); 2*(cos(t)-sin(t))];
f = @(t, y) A*y + B(t);
%f = @(t,y) [ -2 * y(1) + y(2) + 2 * sin(t) ; y(1) - 2 * y(2) + 2 * (cos(t) - sin(t))];
f_exact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];
intv=[0 10];
y0=[2;3];
nmax = 10;
TOL = .001;
N0 = 100;
h0 = 0.1;
jfunc = @(t) A;

%% 
k = f_exact(t);
plot(k(1,:),k(1,:))
%%

[t,Y,ev,loopcount] = mitrapnwt(f,intv,y0,N0,TOL,nmax,jfunc);
plot(Y(1,:),Y(2,:));

%%

[t,Y,ev,loopcount] = mieulerimpnwtpc(f,intv,y0,N0,TOL,nmax,jfunc);
plot(Y(1,:),Y(2,:));

%%

methods = {@mitrapnwtpc, @mieulerimpnwtpc};
names = {'mitrapnwtpc', 'mieulerimpnwtpc'};
k = 'Sistema Rígido';

miplot(methods,names,k,f,f_exact,intv,y0,N0,h0,TOL,nmax,jfunc)